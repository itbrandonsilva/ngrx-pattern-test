import { TesteffectsPage } from './app.po';

describe('testeffects App', () => {
  let page: TesteffectsPage;

  beforeEach(() => {
    page = new TesteffectsPage();
  });

  it('should display message saying app works', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('app works!');
  });
});
