import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { StoreModule, INITIAL_STATE } from '@ngrx/store';
import { EffectsModule } from '@ngrx/effects';

import { StoreManagerService } from './services/store-manager.service';

import { AppComponent } from './app.component';
import { AppEffects } from './app.effects';

import { HomeComponent } from './components/home/home.component';

import { CounterComponent } from './components/counter/counter.component';
import { CounterEffects } from './components/counter/counter.effects';
import { appReducer } from './app.reducers';

const appRoutes: Routes = [
    {path: 'counter', component: CounterComponent },
    {path: '', component: HomeComponent },
];

/*

The primary goal was to ensure that route state/logic is as decoupled as possible. It is only necessary for each route to utilize unique
state keys that don't conflict with other routes. Otherwise, each route need not be concerned about other routes in any way.

@ngrx/effects enable components to be less concerned about business logic by moving a considerable portion of that logic elsewhere.
Each route will have the ability to define async logic utilized throughout the route within its own service class by utilizing "effect" decorators
provided by @ngrx/effects. This allows components to emit simple actions that respresent tasks to be completed, keeping components lean.

The above is achieved with near zero component boilerplate. "container" components (components responsible for rendering routes)
simply need to utilize `StoreManagerService.prepareStore(...)` to initiliaze its reducers and effects, and also automatically cleaning
up resources and effects utilized by the previous route.

*/

@NgModule({
    imports: [
        BrowserModule,
        RouterModule.forRoot(appRoutes),

        StoreModule.provideStore({app: appReducer}),
        EffectsModule.run(AppEffects),
    ],
    declarations: [
        AppComponent,
        HomeComponent,
        CounterComponent,
    ],
    providers: [
        StoreManagerService,
        AppEffects,
        CounterEffects,
        // May or may not be the ideal way to set the initial state on boot
        {
            provide: INITIAL_STATE,
            useValue: {app: {title: 'Application Title'}}
        }
    ],
    bootstrap: [AppComponent]
})
export class AppModule {}
