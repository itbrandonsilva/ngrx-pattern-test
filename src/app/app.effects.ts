import { Injectable } from '@angular/core';
import { Effect, Actions } from '@ngrx/effects';
import { RESET } from './components/counter/counter.reducers';
import { Observable } from 'rxjs';

@Injectable()
export class AppEffects {
    constructor(
        private actions$: Actions
    ) {}

    /**
     * Simply put, the @Effect decoractor defines an observable which receives
     * all actions dispatched to @ngrx/store, where the actions are then filtered by utilizing
     * `ofType`, where we then have the ability to emit further actions.
     * This functionality can be initialized as EffectsModule is imported (which we have done),
     * and also by instantiating new `EffectsSubscription`s (as seen in store-manager.service.ts).
     */

    @Effect() catch$ = this.actions$
        .ofType(RESET)
        .switchMap(action => {
            console.log('Effect emitting RESET_EFFECT');
            return Observable.of({
                type: 'RESET_EFFECT'
            });
        });
}