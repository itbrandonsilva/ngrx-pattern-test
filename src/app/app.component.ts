import { Component } from '@angular/core';
import { Observable } from 'rxjs';
import { Store } from '@ngrx/store';
import { StoreManagerService } from './services/store-manager.service';
import { INCREMENT, DECREMENT, RESET } from './components/counter/counter.reducers';

@Component({
    selector: 'app-component',
    template: `
        <div>
            <button (click)="decrement()">Decrement</button>
            <button (click)="increment()">Increment</button>
        </div>
        <div>
            <button (click)="reset()">Reset Counter</button>
        </div>
        <br>

        <div>{{ title | async }}</div>
        <div>
            <a routerLink="/" routerLinkActive="active" [routerLinkActiveOptions]="{exact: true}">Home</a>
            <a routerLink="/counter" routerLinkActive="active">Counter</a>
        </div>

        <div class="outlet">        
            <router-outlet></router-outlet>
        </div>

        <div>
            <div>State:</div>
            {{ store | async | json }}
        </div>
    `,
    styles: [`
        .outlet {
            margin: 30px;
        }

        .active {
            color: green;
        }
    `]
})
export class AppComponent {
    public title: Observable<string>;

    constructor(
        public store: Store<any>,
        storeManager: StoreManagerService
    ) {
        storeManager.initializeStore();
        this.title = store.select('app', 'title');
        // No need to call StoreManagerService.prepareStore() here.
        // Application-wide data should be ready, after calling initializeStore(), above.
    }

    /**
     * The below three action dispatchers are available in all views for demonstration purposes.
     * The way the actions are handled are based on the active route.
     */

    increment() {
        this.store.dispatch({
            type: INCREMENT
        });
    }

    decrement() {
        this.store.dispatch({
            type: DECREMENT
        });
    }

    reset() {
        this.store.dispatch({
            type: RESET
        });
    }
}