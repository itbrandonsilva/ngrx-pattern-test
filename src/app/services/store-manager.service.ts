import { Store, Action, combineReducers } from '@ngrx/store';
import { EffectsSubscription } from '@ngrx/effects';
import {
    Injectable
} from '@angular/core';
import { appReducer } from '../app.reducers';

@Injectable()
export class StoreManagerService {
    public effectsSubscription;

    constructor(
        private store: Store<any>,
        private subscription: EffectsSubscription,
    ) {}

    /**
     * This is called on app startup to load the store with additional data, IE user data from localStorage
     */
    public initializeStore() {
        /**
         * store.dispatch({type: 'SET_USER', user: user, etc: etc})
         */
    }

    /**
     * This would be called by route container components constructor/ngOnInit to initialize page state and "effects" (@ngrx/effects)
     * 
     * prepareStore updates our reducer tree, causing keys on the store which no longer have a reducer assigned to them to be cleared.
     * Also clears all currently active "effects" and activates each effect in the array that we pass (second argument); 
     * 
     */
    public prepareStore(reducer = {}, effects = []) {
        console.log('prepareStore()', reducer, effects);
        this.store.replaceReducer(this.createReducer(reducer));
        this.setEffects(effects);
    }

    public createReducer(reducer = {}) {
        return combineReducers(Object.assign({app: appReducer}, reducer));
    }

    private setEffects(effects: any[]) {
        this.clearEffects();
        this.store.next({
            type: '@@CLEAR_QUEUE'
        });
        this.effectsSubscription = new EffectsSubscription(this.store, this.subscription, effects);
    }

    private clearEffects() {
        if (this.effectsSubscription)
            this.effectsSubscription.unsubscribe();
    }

    private cleanupStore() {
        this.store.replaceReducer(this.createReducer());
        this.clearEffects();
    }
}