import { Component } from '@angular/core';
import { Store } from '@ngrx/store';
import { StoreManagerService } from '../../services/store-manager.service';
import { Observable } from 'rxjs';
import { homeReducer } from './home.reducers';

@Component({
    selector: 'home-component',
    template: `Home component`,
})
export class HomeComponent {
    public title: Observable<string>;

    constructor(
        public store: Store<any>,
        storeManager: StoreManagerService
    ) {
        storeManager.prepareStore({home: homeReducer});
    }
}