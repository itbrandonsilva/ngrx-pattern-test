import { Component } from '@angular/core';
import { Store } from '@ngrx/store';
import { Observable } from 'rxjs';
import { StoreManagerService } from '../../services/store-manager.service';
import { CounterEffects } from './counter.effects';
import { counterReducer } from './counter.reducers';

@Component({
    selector: 'counter-component',
    template: `
        <div>Counter Component</div>
        <div>Current Count: {{ count | async }}</div>
	`,
})
export class CounterComponent {
    public title: Observable<string>;
    public count: Observable<number>;

    constructor(
        private store: Store<any>,
        storeManager: StoreManagerService,
        private effects: CounterEffects
    ) {
        storeManager.prepareStore({counter: counterReducer}, [effects]);

        this.title = this.store.select('app', 'title');
        this.count = this.store.select('counter');
    }
}