import { Injectable } from '@angular/core';
import { Effect, Actions } from '@ngrx/effects';
import { Observable } from 'rxjs';
import { INCREMENT, DECREMENT } from './counter.reducers';

@Injectable()
export class CounterEffects {
    constructor(
        private actions$: Actions
    ) {}

    @Effect() public increment$ = this.actions$
        .ofType(INCREMENT)
        .switchMap(action => {
            console.log('Effect emitting INCREMENT_EFFECT');
            return Observable.of({
                type: 'INCREMENT_EFFECT'
            });
        });

    @Effect() public decrement$ = this.actions$
        .ofType(DECREMENT)
        .switchMap(action => {
            console.log('Effect emitting DECREMENT_EFFECT');
            return Observable.of({
                type: 'DECREMENT_EFFECT'
            });
        });
}